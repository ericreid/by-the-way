//
//  Constants.h
//  ByTheWay
//
//  Created by Eric Reid on 12/29/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	kGreenCircle = 0,
    kPurpleCircle,
    kRedCircle
} CircleColor;

extern NSString* const kFlurryErrorDeletingLocation;
extern NSString* const kFlurryErrorDeletingReminder;
extern NSString* const kFlurryErrorSavingLocation;
extern NSString* const kFlurryErrorSavingReminder;

extern NSString* const kFlurryEventAboutViewed;
extern NSString* const kFlurryEventAddLocationTapped;
extern NSString* const kFlurryEventAddLocationFromAddress;
extern NSString* const kFlurryEventAddLocationFromContact;
extern NSString* const kFlurryEventAddLocationFromCurrentPosition;
extern NSString* const kFlurryEventAddLocationFromMapCenter;
extern NSString* const kFlurryEventAddReminderTapped;
extern NSString* const kFlurryEventAppDidBecomeActive;
extern NSString* const kFlurryEventAppDidEnterBackground;
extern NSString* const kFlurryEventAppDidFinishLaunching;
extern NSString* const kFlurryEventAppDidReceiveLocalNotification;
extern NSString* const kFlurryEventAppWillEnterForeground;
extern NSString* const kFlurryEventAppWillResignActive;
extern NSString* const kFlurryEventAppWillTerminate;
extern NSString* const kFlurryEventFeedbackSent;
extern NSString* const kFlurryEventFeedbackTapped;
extern NSString* const kFlurryEventGeofenceTriggered;
extern NSString* const kFlurryEventLocationColorChanged;
extern NSString* const kFlurryEventLocationDeleted;
extern NSString* const kFlurryEventLocationNicknameChanged;
extern NSString* const kFlurryEventLocationRadiusChanged;
extern NSString* const kFlurryEventLocationSaved;
extern NSString* const kFlurryEventNotificationReceived;
extern NSString* const kFlurryEventReminderDeleted;
extern NSString* const kFlurryEventReminderDirectionChanged;
extern NSString* const kFlurryEventReminderEarliestTimeChanged;
extern NSString* const kFlurryEventReminderLatestTimeChanged;
extern NSString* const kFlurryEventReminderNicknameChanged;
extern NSString* const kFlurryEventReminderSaved;
extern NSString* const kFlurryEventWebsiteTapped;

