//
//  LocationDetailViewController.m
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "LocationDetailViewController.h"
#import "LocationMapOverlay.h"
#import "LocationAnnotation.h"

static const double kViewSpanDelta = 0.0075;

@interface LocationDetailViewController ()
- (void)updatePreviewMap;
@end

@implementation LocationDetailViewController
@synthesize managedObjectContext;
@synthesize location;
@synthesize nicknameTextField;
@synthesize radiusSlider;
@synthesize radiusLabel;
@synthesize previewMapView;
@synthesize colorControl;


- (void)dealloc {
    [managedObjectContext release], managedObjectContext = nil;
    [location release], location = nil;
    [nicknameTextField release], nicknameTextField = nil;
    [radiusSlider release], radiusSlider = nil;
    [radiusLabel release], radiusLabel = nil;
    [previewMapView release], previewMapView = nil;
    [colorControl release], colorControl = nil;
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.nicknameTextField = nil;
    self.radiusSlider = nil;
    self.radiusLabel = nil;
    self.previewMapView = nil;
    self.colorControl = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)] autorelease];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(delete)] autorelease]; 
    
    self.nicknameTextField.text = self.location.nickname;
    self.colorControl.selectedSegmentIndex = self.location.colorValue;
    self.radiusSlider.value = self.location.radiusValue;
    self.radiusLabel.text = [NSString stringWithFormat:@"%d meters", self.location.radiusValue];
    [self updatePreviewMap];
}

- (void)save {
    if(self.nicknameTextField.text == nil || self.nicknameTextField.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                            message:@"The nickname cannot be blank"
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    } else {        
        [[LocationManager sharedInstance] saveGeofenceAtLatitude:self.location.latitudeValue longitude:self.location.longitudeValue radius:self.radiusSlider.value];
        
        self.location.nickname = self.nicknameTextField.text;
        self.location.radiusValue = self.radiusSlider.value;
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            [FlurryAPI logError:kFlurryErrorSavingLocation message:@"Could not save location" error:error];
        } 
        [FlurryAPI logEvent:kFlurryEventLocationSaved];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)delete {
    [[LocationManager sharedInstance] deleteGeofenceAtLatitude:self.location.latitudeValue longitude:self.location.longitudeValue];
    [self.managedObjectContext deleteObject:self.location];
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        [FlurryAPI logError:kFlurryErrorDeletingLocation message:@"Could not delete location" error:error];
    } 
    [FlurryAPI logEvent:kFlurryEventLocationDeleted];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [FlurryAPI logEvent:kFlurryEventLocationNicknameChanged withParameters:[NSDictionary dictionaryWithObject:textField.text forKey:@"New Nickname"]];
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    if ([overlay isKindOfClass:[LocationMapOverlay class]]) {
        LocationMapOverlay* locationOverlay = (LocationMapOverlay*)overlay;
        return [locationOverlay viewForOverlay];
    }
    
    return nil;
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
    if(annotation != self.previewMapView.userLocation)
    {
        LocationAnnotation *locationAnnotation = (LocationAnnotation*)annotation;
        static NSString *viewIdentifier = @"annotationView";
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:viewIdentifier];
        if (annotationView == nil) {
            annotationView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewIdentifier] autorelease];
        }
        switch (locationAnnotation.location.colorValue) {
            case kGreenCircle:
                annotationView.pinColor = MKPinAnnotationColorGreen;
                break;
            case kPurpleCircle:
                annotationView.pinColor = MKPinAnnotationColorPurple;
                break;
            case kRedCircle:
                annotationView.pinColor = MKPinAnnotationColorRed;
                break;
            default:
                break;
        }
        annotationView.animatesDrop = NO;
        annotationView.canShowCallout = NO;
        return annotationView;
    } else {
        return nil;
    }
}

- (void)updatePreviewMap {
    @synchronized(self.previewMapView) {
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        span.latitudeDelta = kViewSpanDelta;
        span.longitudeDelta = kViewSpanDelta;    
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = self.location.latitudeValue;
        coordinate.longitude = self.location.longitudeValue;
        
        region.span = span;
        region.center = coordinate;
        [self.previewMapView setRegion:region animated:NO];
        
        [self.previewMapView removeAnnotations:self.previewMapView.annotations];
        [self.previewMapView removeOverlays:self.previewMapView.overlays];
        
        LocationMapOverlay *mapOverlay = [[LocationMapOverlay alloc] initWithLocation:self.location];
        mapOverlay.color = self.location.colorValue;
        [self.previewMapView addOverlay:mapOverlay];
        [mapOverlay release];
        
        LocationAnnotation *annotation = [[LocationAnnotation alloc] initWithCoordinate:coordinate];
        [self.previewMapView addAnnotation:annotation];
        annotation.location = location;
        annotation.title = location.nickname;
        [annotation release];
    }
}

#pragma mark -
#pragma mark IBAction

- (IBAction)radiusSliderMoved {
    int roundedValue = (int)((((int)self.radiusSlider.value + 25)/50) * 50);
    self.radiusSlider.value = roundedValue;
    self.radiusLabel.text = [NSString stringWithFormat:@"%d meters", roundedValue];
    self.location.radiusValue = self.radiusSlider.value;
    [self updatePreviewMap];
    [FlurryAPI logEvent:kFlurryEventLocationRadiusChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithDouble:self.radiusSlider.value] forKey:@"New Radius"]];
}

- (IBAction)hideKeyboard {
    [self.nicknameTextField resignFirstResponder];
}

- (IBAction)colorChanged {
    self.location.colorValue = self.colorControl.selectedSegmentIndex;
    [self updatePreviewMap];
    [FlurryAPI logEvent:kFlurryEventLocationColorChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:self.colorControl.selectedSegmentIndex] forKey:@"New Color"]];
}

@end
