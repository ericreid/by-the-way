//
//  ReminderListViewControllerWithAds.m
//  ByTheWay
//
//  Created by Eric Reid on 3/31/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import "ReminderListViewControllerWithAds.h"
#import "ByTheWayAppDelegate.h"


static NSString* const kAdWhirlKey = @"2abbec8c00194db29fc2c1e7c500fecb";

@implementation ReminderListViewControllerWithAds
@synthesize adContainerView;

- (void)dealloc {
    [adContainerView release], adContainerView = nil;
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.adContainerView = nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
    [self.adContainerView addSubview:adWhirlView];
    adWhirlView.frame = self.adContainerView.frame;
}

#pragma mark -
#pragma mark AdWhirl

- (NSString *)adWhirlApplicationKey {
    return kAdWhirlKey;
}

- (UIViewController *)viewControllerForPresentingModalView {    
    return [(ByTheWayAppDelegate*)[[UIApplication sharedApplication] delegate] tabBarController];
}

- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
/*    [UIView beginAnimations:@"AdWhirlDelegate.adWhirlDidReceiveAd:"
                    context:nil];

    [UIView setAnimationDuration:0.7];
    
    CGSize adSize = [adWhirlView actualAdSize];
    CGRect newFrame = adWhirlView.frame;
    newFrame.size = adSize;
    newFrame.origin.x = (self.view.bounds.size.width - adSize.width)/ 2;
    adWhirlView.frame = newFrame;
    [UIView commitAnimations];*/
}

- (void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
}

@end
