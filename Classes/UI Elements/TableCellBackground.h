//
//  TableCellBackground.h
//  ByTheWay
//
//  Created by Eric Reid on 3/6/10.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TableCellBackground : UIView {

}

- (id)initWithTopColor:(UIColor*)topColor bottomColor:(UIColor*)bottomColor borderColor:(UIColor*)borderColor;

@end
