//
//  LocationAnnotation.h
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LocationAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
}
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, retain) Location *location;

- (id)initWithCoordinate:(CLLocationCoordinate2D)c;

@end
