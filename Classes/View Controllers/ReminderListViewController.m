//
//  ReminderListViewController.m
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "ReminderListViewController.h"
#import "ReminderDetailViewController.h"
#import "TableCellBackground.h"
#import "AboutViewController.h"

static NSString* const kAm = @"AM";
static NSString* const kPm = @"PM";

@interface ReminderListViewController ()
@property (nonatomic, retain) NSArray *locations;
@property (nonatomic, retain) NSDictionary *remindersByLocation;
- (void)updateData;
@end

@implementation ReminderListViewController
@synthesize managedObjectContext;
@synthesize noRemindersLabel;
@synthesize remindersTableView;

@synthesize locations;
@synthesize remindersByLocation;

- (void)dealloc {
    [managedObjectContext release], managedObjectContext = nil;
    [noRemindersLabel release], noRemindersLabel = nil;
    [remindersTableView release], remindersTableView = nil;
    
    [locations release], locations = nil;
    [remindersByLocation release], remindersByLocation = nil;
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton* infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [infoButton addTarget:self action:@selector(aboutTapped) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *aboutButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
    self.navigationItem.leftBarButtonItem = aboutButton;
    
    [aboutButton release];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateData];
    [self.remindersTableView reloadData];
}

- (void)updateData {
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    [request setEntity:[NSEntityDescription entityForName:[Location entityName] inManagedObjectContext:self.managedObjectContext]];
    self.locations = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    NSMutableDictionary *remindersDictionary = [NSMutableDictionary dictionary];
    
    int numReminders = 0;
    for(Location *location in self.locations) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"subject" ascending:YES];
        NSArray* sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
        NSArray *reminders = [[location.reminders allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        [remindersDictionary setObject:reminders forKey:location.objectID];
        numReminders += reminders.count;
    }
    
    self.remindersByLocation = remindersDictionary;
    
    DLog(@"Locations count: %d", self.locations.count);
    DLog(@"Geofences count: %d", [[LocationManager sharedInstance] allGeofences].count);
    DLog(@"Reminders count: %d", numReminders);
    
    if(numReminders == 0) {
        [[LocationManager sharedInstance] stopUpdatingLocation];
    } else {
        [[LocationManager sharedInstance] startUpdatingLocation];
    }
    
    if(self.locations.count == 0) {
        self.noRemindersLabel.hidden = NO;
    } else {
        self.noRemindersLabel.hidden = YES;
    }
}

- (void)addNewReminder {
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.locations.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Location *sectionLocation = [self.locations objectAtIndex:section];
    TableHeaderView *header = [[[TableHeaderView alloc] init] autorelease];        
    header.location = sectionLocation;
    header.delegate = self;
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Location *sectionLocation = [self.locations objectAtIndex:section];
    NSArray *remindersForSection = [self.remindersByLocation objectForKey:sectionLocation.objectID];
    return remindersForSection.count;
}

- (NSString*)formattedTime:(long)time {
    int hour = time / 3600;
    int minute = time / 60 - hour * 60;
    NSString *ampm = nil;
    if(hour == 0) {
        hour = 12;
        ampm = kAm;
    } else if(hour > 24) {
        hour -= 24;
        ampm = kAm;
    } else if(hour == 24) {
        hour = 12;
        ampm = kAm;
    } else if (hour > 12) {
        hour -= 12;
        ampm = kPm;
    } else if(hour == 12) {
        ampm = kPm;
    } else {
        ampm = kAm;
    }
    return [NSString stringWithFormat:@"%d:%02d %@", hour, minute, ampm];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        UIColor *unselectedColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        UIColor *selectedColor = [UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0];
        UIColor *borderColor = [UIColor colorWithRed:0.875 green:0.875 blue:0.875 alpha:1.0];
        
        cell.backgroundView = [[[TableCellBackground alloc] initWithTopColor:unselectedColor
                                                                 bottomColor:unselectedColor
                                                                 borderColor:borderColor] autorelease];
        cell.selectedBackgroundView = [[[TableCellBackground alloc] initWithTopColor:selectedColor
                                                                         bottomColor:selectedColor
                                                                         borderColor:borderColor] autorelease];
    }
    
    Location *sectionLocation = [self.locations objectAtIndex:indexPath.section];
    NSArray *remindersForSection = [self.remindersByLocation objectForKey:sectionLocation.objectID];
    Reminder *reminder = [remindersForSection objectAtIndex:indexPath.row];

    NSString *arriveLeave = reminder.enteringValue ? @"(A)" : @"(L)";
    cell.textLabel.text = [NSString stringWithFormat: @"%@ %@", arriveLeave, reminder.subject];
    
    if(reminder.earliestValue >= 0 && reminder.latestValue >= 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Between %@ and %@", [self formattedTime:reminder.earliestValue], [self formattedTime:reminder.latestValue]];
    } else if (reminder.earliestValue >= 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"After %@", [self formattedTime:reminder.earliestValue]];
    } else if (reminder.latestValue >= 0) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Before %@", [self formattedTime:reminder.latestValue]];
    } else {
        cell.detailTextLabel.text = @"Anytime";
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Location *sectionLocation = [self.locations objectAtIndex:indexPath.section];
        NSArray *remindersForSection = [self.remindersByLocation objectForKey:sectionLocation.objectID];
        Reminder *reminder = [remindersForSection objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:reminder];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
        [self updateData];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }    
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Location *sectionLocation = [self.locations objectAtIndex:indexPath.section];
    NSArray *remindersForSection = [self.remindersByLocation objectForKey:sectionLocation.objectID];
    Reminder *reminder = [remindersForSection objectAtIndex:indexPath.row];
    
    ReminderDetailViewController *controller = [[ReminderDetailViewController alloc] init];
    controller.reminder = reminder;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

#pragma mark - 
#pragma mark ReminderListDelegate

- (void)addTappedForLocation:(Location*)location {
    [FlurryAPI logEvent:kFlurryEventAddReminderTapped];
    Reminder *newReminder = [Reminder insertInManagedObjectContext:self.managedObjectContext];
    newReminder.location = location;
    
    ReminderDetailViewController *controller = [[ReminderDetailViewController alloc] init];
    controller.reminder = newReminder;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

#pragma mark -
#pragma mark About

- (void)aboutTapped {
    [FlurryAPI logEvent:kFlurryEventAboutViewed];
    AboutViewController *controller = [[AboutViewController alloc] init];
    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:controller animated:YES];
    [controller release];
}


@end

