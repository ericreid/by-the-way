#import "Location.h"

@implementation Location

- (CLLocationCoordinate2D)coordinate {
    CLLocationCoordinate2D retVal;
    retVal.latitude = self.latitudeValue;
    retVal.longitude = self.longitudeValue;
    return retVal;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coord {
    self.latitudeValue = [[NSString stringWithFormat:@"%.5f", coord.latitude] doubleValue];
    self.longitudeValue = [[NSString stringWithFormat:@"%.5f", coord.longitude] doubleValue];
}

+ (NSArray*)allLocationsInManagedObjectContext:(NSManagedObjectContext*)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Location entityInManagedObjectContext:context];
    [request setEntity:entity];
    NSArray *array = [context executeFetchRequest:request error:nil];
    [request release];
    
    return array;
}

+ (Location*)locationAtLatitude:(double)latitude longitude:(double)longitude inManagedObjectContext:(NSManagedObjectContext*)context {
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Location entityInManagedObjectContext:context];
    [request setEntity:entity];
    
    NSString *predicateString = [NSString stringWithFormat:@"latitude = '%.5f' AND longitude = '%.5f'", latitude, longitude];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    NSArray *array = [context executeFetchRequest:request error:nil];
    [request release];
    
    if(array.count > 0) {
        return [array objectAtIndex:0];
    } else {
        DLog(@"Geofence fired, but no location found!!");
        DLog(@"Geofence = %@", predicateString);
        
        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
        [request setEntity:[NSEntityDescription entityForName:[Location entityName] inManagedObjectContext:context]];
        DLog(@"Locations = %@", [context executeFetchRequest:request error:nil]);
    }
    
    return nil;
}

@end
