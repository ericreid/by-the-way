// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Location.h instead.

#import <CoreData/CoreData.h>


@class Reminder;







@interface LocationID : NSManagedObjectID {}
@end

@interface _Location : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LocationID*)objectID;



@property (nonatomic, retain) NSString *nickname;

//- (BOOL)validateNickname:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *longitude;

@property double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *latitude;

@property double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *color;

@property short colorValue;
- (short)colorValue;
- (void)setColorValue:(short)value_;

//- (BOOL)validateColor:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *radius;

@property short radiusValue;
- (short)radiusValue;
- (void)setRadiusValue:(short)value_;

//- (BOOL)validateRadius:(id*)value_ error:(NSError**)error_;




@property (nonatomic, retain) NSSet* reminders;
- (NSMutableSet*)remindersSet;




@end

@interface _Location (CoreDataGeneratedAccessors)

- (void)addReminders:(NSSet*)value_;
- (void)removeReminders:(NSSet*)value_;
- (void)addRemindersObject:(Reminder*)value_;
- (void)removeRemindersObject:(Reminder*)value_;

@end

@interface _Location (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveNickname;
- (void)setPrimitiveNickname:(NSString*)value;


- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;


- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;


- (NSNumber*)primitiveColor;
- (void)setPrimitiveColor:(NSNumber*)value;

- (short)primitiveColorValue;
- (void)setPrimitiveColorValue:(short)value_;


- (NSNumber*)primitiveRadius;
- (void)setPrimitiveRadius:(NSNumber*)value;

- (short)primitiveRadiusValue;
- (void)setPrimitiveRadiusValue:(short)value_;




- (NSMutableSet*)primitiveReminders;
- (void)setPrimitiveReminders:(NSMutableSet*)value;


@end
