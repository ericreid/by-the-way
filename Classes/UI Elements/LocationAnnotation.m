//
//  LocationAnnotation.m
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "LocationAnnotation.h"

@implementation LocationAnnotation
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize location;

- (void)dealloc {
    [title release], title = nil;
    [subtitle release], subtitle = nil;
    [location release], location = nil;
    
    [super dealloc];
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)c {
    coordinate=c;
    return self;
}

@end
