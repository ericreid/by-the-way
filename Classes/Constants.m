//
//  Constants.m
//  ByTheWay
//
//  Created by Eric Reid on 12/29/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "Constants.h"

NSString* const kFlurryErrorDeletingLocation = @"Error Deleting Location";
NSString* const kFlurryErrorDeletingReminder = @"Error Deleting Reminder";
NSString* const kFlurryErrorSavingLocation = @"Error Saving Location";
NSString* const kFlurryErrorSavingReminder = @"Error Saving Reminder";

NSString* const kFlurryEventAboutViewed = @"About Viewed";
NSString* const kFlurryEventAddLocationTapped = @"Add Location Button Tapped";
NSString* const kFlurryEventAddLocationFromAddress = @"Add Location From Address Tapped";
NSString* const kFlurryEventAddLocationFromContact = @"Add Location From Contact Tapped";
NSString* const kFlurryEventAddLocationFromCurrentPosition = @"Add Location From Current Position Tapped";
NSString* const kFlurryEventAddLocationFromMapCenter = @"Add Location at Center of Map Tapped";
NSString* const kFlurryEventAddReminderTapped = @"Add Reminder Button Tapped";
NSString* const kFlurryEventAppDidBecomeActive = @"App Became Active";
NSString* const kFlurryEventAppDidEnterBackground = @"App Entered Background";
NSString* const kFlurryEventAppDidFinishLaunching = @"App Finished Launching";
NSString* const kFlurryEventAppDidReceiveLocalNotification = @"App Received Local Notification";
NSString* const kFlurryEventAppWillEnterForeground = @"App Entered Foreground";
NSString* const kFlurryEventAppWillResignActive = @"App Resigned Active";
NSString* const kFlurryEventAppWillTerminate = @"App Terminated";
NSString* const kFlurryEventFeedbackSent = @"Feedback Button Sent";
NSString* const kFlurryEventFeedbackTapped = @"Feedback Button Tapped";
NSString* const kFlurryEventGeofenceTriggered = @"Geofence Triggered";
NSString* const kFlurryEventLocationColorChanged = @"Location Color Changed";
NSString* const kFlurryEventLocationDeleted = @"Location Deleted";
NSString* const kFlurryEventLocationNicknameChanged = @"Nickname Changed";
NSString* const kFlurryEventLocationRadiusChanged = @"Location Radius Changed";
NSString* const kFlurryEventLocationSaved = @"Location Saved";
NSString* const kFlurryEventNotificationReceived = @"Notification Received";
NSString* const kFlurryEventReminderDeleted = @"Reminder Deleted";
NSString* const kFlurryEventReminderDirectionChanged = @"Reminder Direction Changed";
NSString* const kFlurryEventReminderEarliestTimeChanged = @"Reminder Earliest Time Changed";
NSString* const kFlurryEventReminderLatestTimeChanged = @"Reminder Latest Time Changed";
NSString* const kFlurryEventReminderNicknameChanged = @"Reminder Nickname Changed";
NSString* const kFlurryEventReminderSaved = @"Reminder Saved";
NSString* const kFlurryEventWebsiteTapped = @"Website Button Tapped";