//
//  ReminderDetailViewController.m
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "ReminderDetailViewController.h"

static NSString* const kAm = @"AM";
static NSString* const kPm = @"PM";

static NSString* const kLeftSegmentHighlightedFileName = @"left_segmented_control.png";
static NSString* const kLeftSegmentBackgroundFileName = @"left_segmented_background.png";
static NSString* const kRightSegmentHighlightedFileName = @"right_segmented_control.png";
static NSString* const kRightSegmentBackgroundFileName = @"right_segmented_background.png";

@interface ReminderDetailViewController ()
@property (nonatomic) BOOL earliestTimePickerShowing;
@property (nonatomic) BOOL latestTimePickerShowing;
@property (nonatomic, retain) NSString *subject;
- (NSString*)formattedTime:(long)time;
@end

@implementation ReminderDetailViewController
@synthesize reminder;
@synthesize arriveButton;
@synthesize earliestAnytimeButton;
@synthesize earliestSpecificButton;
@synthesize latestAnytimeButton;
@synthesize latestSpecificButton;
@synthesize leaveButton;
@synthesize timePicker;
@synthesize subjectField;

@synthesize earliestTimePickerShowing;
@synthesize latestTimePickerShowing;
@synthesize subject;

- (void)dealloc {
    [reminder release], reminder = nil;
    [arriveButton release], arriveButton = nil;
    [earliestAnytimeButton release], earliestAnytimeButton = nil;
    [earliestSpecificButton release], earliestSpecificButton = nil;
    [latestAnytimeButton release], latestAnytimeButton = nil;
    [latestSpecificButton release], latestSpecificButton = nil;
    [leaveButton release], leaveButton = nil;
    [timePicker release], timePicker = nil;
    [subjectField release], subjectField = nil;
    
    [subject release], subject = nil;
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.arriveButton = nil;
    self.earliestAnytimeButton = nil;
    self.earliestSpecificButton = nil;
    self.latestAnytimeButton = nil;
    self.latestSpecificButton = nil;
    self.leaveButton = nil;
    self.timePicker = nil;
    self.subjectField = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save)] autorelease];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(delete)] autorelease]; 
    
    self.subject = self.reminder.subject;
}

- (void)viewWillAppear:(BOOL)animated {
    self.subjectField.text = self.subject;
    
    if(self.reminder.earliestValue > -1) {
        [self.earliestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
        [self.earliestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
        [self.earliestSpecificButton setTitle:[self formattedTime:self.reminder.earliestValue] forState:UIControlStateNormal];
    }
    
    if(self.reminder.latestValue > -1) {
        [self.latestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
        [self.latestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
        [self.latestSpecificButton setTitle:[self formattedTime:self.reminder.latestValue] forState:UIControlStateNormal];
    }
    
    if(!self.reminder.enteringValue) {
        [self.arriveButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
        [self.leaveButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
    }
}

- (NSString*)formattedTime:(long)time {
    int hour = time / 3600;
    int minute = time / 60 - hour * 60;
    NSString *ampm = nil;
    if(hour == 0) {
        hour = 12;
        ampm = kAm;
    } else if(hour > 24) {
        hour -= 24;
        ampm = kAm;
    } else if(hour == 24) {
        hour = 12;
        ampm = kAm;
    } else if (hour > 12) {
        hour -= 12;
        ampm = kPm;
    } else if(hour == 12) {
        ampm = kPm;
    } else {
        ampm = kAm;
    }
    return [NSString stringWithFormat:@"%d:%02d %@", hour, minute, ampm];
}

- (void)save {
    if(self.subjectField.text == nil || self.subjectField.text.length == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                            message:@"The subject cannot be blank"
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    } else if(self.reminder.latestValue != -1 && self.reminder.latestValue <= self.reminder.earliestValue) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                            message:@"'After' time must be later than 'Before' time"
                                                           delegate:nil 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    } else {
        self.reminder.subject = self.subjectField.text;
        
        NSError *error = nil;
        if (![reminder.managedObjectContext save:&error]) {
            DLog(@"Unresolved error %@, %@", error, [error userInfo]);
            [FlurryAPI logError:kFlurryErrorSavingReminder message:@"Could not save reminder" error:error];
        } 
        [FlurryAPI logEvent:kFlurryEventReminderSaved];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)delete {
    NSManagedObjectContext *moc = self.reminder.managedObjectContext;
    [moc deleteObject:self.reminder];
    NSError *error = nil;
    if (![moc save:&error]) {
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        [FlurryAPI logError:kFlurryErrorDeletingReminder message:@"Could not delete reminder" error:error];
    } 
    [FlurryAPI logEvent:kFlurryEventReminderDeleted];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)hideTimePicker {
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{     
        CGRect newViewFrame = self.view.frame;
        newViewFrame.origin.y = 0;
        newViewFrame.size.height = 416;
        self.view.frame = newViewFrame;
        
        CGRect newPickerFrame = self.timePicker.frame;
        newPickerFrame.origin.y = 416;
        self.timePicker.frame = newPickerFrame;
    } completion:nil]; 
    self.earliestTimePickerShowing = NO;
    self.latestTimePickerShowing = NO;
}


- (void)hideKeyboard {
    [self.subjectField resignFirstResponder];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [FlurryAPI logEvent:kFlurryEventReminderNicknameChanged withParameters:[NSDictionary dictionaryWithObject:textField.text forKey:@"New Nickname"]];
    self.subject = self.subjectField.text;
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark IBActions

- (IBAction)arriveTapped {
    [FlurryAPI logEvent:kFlurryEventReminderDirectionChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"Reminder Is For Entering"]];
    [self hideKeyboard];
    [self hideTimePicker];
    self.reminder.enteringValue = YES;
    [self.arriveButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentHighlightedFileName] forState:UIControlStateNormal];
    [self.leaveButton setBackgroundImage:[UIImage imageNamed:kRightSegmentBackgroundFileName] forState:UIControlStateNormal];
}

- (IBAction)backgroundTapped {
    [self hideKeyboard];
    [self hideTimePicker];
}

- (IBAction)earliestAnytimeTapped {
    [FlurryAPI logEvent:kFlurryEventReminderEarliestTimeChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:-1] forKey:@"New Earliest Time"]];
    [self hideKeyboard];
    [self hideTimePicker];
    [self.earliestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentHighlightedFileName] forState:UIControlStateNormal];
    [self.earliestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentBackgroundFileName] forState:UIControlStateNormal];
    self.reminder.earliestValue = -1;
    [self.earliestSpecificButton setTitle:@"Specific Time" forState:UIControlStateNormal];
}

- (IBAction)earliestSpecificTapped {
    [self.earliestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
    [self.earliestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
    
    [self hideKeyboard];
    if(self.earliestTimePickerShowing) {
        [self hideTimePicker];
    } else {
        if(self.reminder.earliestValue >= 0) {
            [self.timePicker setDate:[TimeUtils dateFromSecondsInDay:self.reminder.earliestValue] animated:NO];  
        } else {
            self.reminder.earliestValue = 0;
            [self.timePicker setDate:[TimeUtils dateFromSecondsInDay:0] animated:NO]; 
            [self.earliestSpecificButton setTitle:[self formattedTime:0] forState:UIControlStateNormal];
        }
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{     
            CGRect newViewFrame = self.view.frame;
            newViewFrame.origin.y = -60;
            newViewFrame.size.height += 60;
            self.view.frame = newViewFrame;
            
            CGRect newPickerFrame = self.timePicker.frame;
            newPickerFrame.origin.y = 260;
            self.timePicker.frame = newPickerFrame;
        } completion:nil]; 
        self.earliestTimePickerShowing = YES;
        self.latestTimePickerShowing = NO;
    }
}

- (IBAction)latestAnytimeTapped {
    [FlurryAPI logEvent:kFlurryEventReminderLatestTimeChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:-1] forKey:@"New Latest Time"]];
    [self hideKeyboard];
    [self hideTimePicker];
    [self.latestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentHighlightedFileName] forState:UIControlStateNormal];
    [self.latestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentBackgroundFileName] forState:UIControlStateNormal];
    self.reminder.latestValue = -1;
    [self.latestSpecificButton setTitle:@"Specific Time" forState:UIControlStateNormal];
}

- (IBAction)latestSpecificTapped {
    [self.latestAnytimeButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
    [self.latestSpecificButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
    
    [self hideKeyboard];
    if(self.latestTimePickerShowing) {
        [self hideTimePicker];
    } else {
        if(self.reminder.latestValue >= 0) {
            [self.timePicker setDate:[TimeUtils dateFromSecondsInDay:self.reminder.latestValue] animated:NO];  
        } else {
            self.reminder.latestValue = 0;
            [self.timePicker setDate:[TimeUtils dateFromSecondsInDay:0] animated:NO]; 
            [self.latestSpecificButton setTitle:[self formattedTime:0] forState:UIControlStateNormal];
        }
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{     
            CGRect newViewFrame = self.view.frame;
            newViewFrame.origin.y = -260;
            newViewFrame.size.height += 260;
            self.view.frame = newViewFrame;
            
            CGRect newPickerFrame = self.timePicker.frame;
            newPickerFrame.origin.y = 460;
            self.timePicker.frame = newPickerFrame;
        } completion:nil]; 
        self.latestTimePickerShowing = YES;
        self.earliestTimePickerShowing = NO;
    }
}

- (IBAction)leaveTapped {
    [FlurryAPI logEvent:kFlurryEventReminderDirectionChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:@"Reminder Is For Entering"]];
    [self hideKeyboard];
    [self hideTimePicker];
    self.reminder.enteringValue = NO;
    [self.arriveButton setBackgroundImage:[UIImage imageNamed:kLeftSegmentBackgroundFileName] forState:UIControlStateNormal];
    [self.leaveButton setBackgroundImage:[UIImage imageNamed:kRightSegmentHighlightedFileName] forState:UIControlStateNormal];
}

- (IBAction)timePickerChanged {
    int secondsInDay = [TimeUtils secondsInDayFromDate:self.timePicker.date];
    
    if(self.earliestTimePickerShowing) {
        [FlurryAPI logEvent:kFlurryEventReminderEarliestTimeChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:secondsInDay] forKey:@"New Earliest Time"]];
        self.reminder.earliestValue = secondsInDay;
        [self.earliestSpecificButton setTitle:[self formattedTime:secondsInDay] forState:UIControlStateNormal];
    } else {
        [FlurryAPI logEvent:kFlurryEventReminderLatestTimeChanged withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:secondsInDay] forKey:@"New Latest Time"]];
        self.reminder.latestValue = secondsInDay;
        [self.latestSpecificButton setTitle:[self formattedTime:secondsInDay] forState:UIControlStateNormal];
    }
}

@end
