//
//  ByTheWayAppDelegate.m
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "ByTheWayAppDelegate.h"
#import "LocationsViewController.h"
#import "ReminderListViewController.h"

static NSString* const kFlurryApiKey = @"72XDBYZE4RGNNUEKF45Q";
static NSString* const kByTheWayGeofences = @"btw_geofences";

@interface ByTheWayAppDelegate ()
- (NSURL *)applicationDocumentsDirectory;
- (void)checkForOldGeofences;
@end

@implementation ByTheWayAppDelegate

@synthesize window;
@synthesize tabBarController;
@synthesize reminderListViewController;
@synthesize locationsViewController;

- (void)dealloc {
    [window release], window = nil;
    [tabBarController release], tabBarController = nil;
    [reminderListViewController release], reminderListViewController = nil;
    [locationsViewController release], locationsViewController = nil;
    [super dealloc];
}

#pragma mark Exception Handler

void uncaughtExceptionHandler(NSException *exception) {
    [FlurryAPI logError:@"Uncaught" message:@"Crash!" exception:exception];
}

- (void)checkForOldGeofences {
    NSNumber *btwGeofences = [[NSUserDefaults standardUserDefaults] objectForKey:kByTheWayGeofences];
    if(!btwGeofences) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:kByTheWayGeofences];
        for(Location *location in [Location allLocationsInManagedObjectContext:self.managedObjectContext]) {
            [[LocationManager sharedInstance] saveGeofenceAtLatitude:location.latitudeValue longitude:location.longitudeValue radius:location.radiusValue];
        }
    }
}


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {  
    [self checkForOldGeofences];
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    [FlurryAPI startSession:kFlurryApiKey];
    [FlurryAPI logAllPageViews:self.tabBarController];
    [FlurryAPI logEvent:kFlurryEventAppDidFinishLaunching];

    [LocationManager sharedInstance].geofenceDelegate = self;
    [[LocationManager sharedInstance] startUpdatingLocation];

    self.reminderListViewController.managedObjectContext = self.managedObjectContext;
    self.locationsViewController.managedObjectContext = self.managedObjectContext;
    
    [self.window addSubview:tabBarController.view];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    [FlurryAPI logEvent:kFlurryEventNotificationReceived];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:notification.alertBody delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [alert release];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    [FlurryAPI logEvent:kFlurryEventAppWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [FlurryAPI logEvent:kFlurryEventAppDidEnterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [FlurryAPI logEvent:kFlurryEventAppWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FlurryAPI logEvent:kFlurryEventAppDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [FlurryAPI logEvent:kFlurryEventAppWillTerminate];
}

#pragma mark -
#pragma mark VPGeofenceDelegate

- (void)geofenceTriggered:(CLRegion*)geofence entered:(BOOL)entered  {
    [FlurryAPI logEvent:kFlurryEventGeofenceTriggered];
    Location *matchingLocation = [Location locationAtLatitude:geofence.center.latitude longitude:geofence.center.longitude inManagedObjectContext:self.managedObjectContext];
    
    if(matchingLocation) {    
        NSString *arrivedLeft = entered ? @"Arrived at" : @"Left";
        DLog(@"Geofence Triggered: %@ location '%@'", arrivedLeft, matchingLocation.nickname);
        for(Reminder *reminder in matchingLocation.reminders) {
            DLog(@"Reminder: %@", reminder);

            NSUInteger secondsInDay = [TimeUtils secondsInDayFromDate:[NSDate date]];
            if(entered && reminder.enteringValue == NO) {
                DLog(@"Geofence is entering, but reminder is for exiting");
                continue;
            } else if (!entered && reminder.enteringValue == YES) {
                DLog(@"Geofence is exiting, but reminder is for entering");
                continue;
            } else if(reminder.earliestValue > secondsInDay) {
                DLog(@"Reminder is of the right type, but it is currently too early in the day");
                continue;
            } else if(reminder.latestValue != -1 && reminder.latestValue < secondsInDay) {
                DLog(@"Reminder is of the right type, but it is currently too late in the day"); 
                continue;
            } else {
                DLog(@"Found a matching reminder!");
                NSString *message = [NSString stringWithFormat:@"%@ %@: Remember to %@", arrivedLeft, matchingLocation.nickname, reminder.subject];
                
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                if(!notification) {
                    DLog(@"Error creating notification!!!");
                    return;
                }
                
                notification.alertBody = message;
                notification.soundName = UILocalNotificationDefaultSoundName;
                notification.alertAction = nil;
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
                [notification release];
            }
        }
    } else {
        DLog(@"Geofence fired, but no matching location found!!");
        DLog(@"Geofence = %@", geofence);
        
        NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
        [request setEntity:[Location entityInManagedObjectContext:self.managedObjectContext]];
        DLog(@"Locations = %@", [self.managedObjectContext executeFetchRequest:request error:nil]);
    }
}

#pragma mark -
#pragma mark Core Data stack
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}

- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:@"ByTheWay" ofType:@"momd"];
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return managedObjectModel_;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ByTheWay.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        DLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return persistentStoreCoordinator_;
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

#pragma mark Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

