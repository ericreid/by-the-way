//
//  LocationsViewController.h
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LocationsViewController : UIViewController <MKMapViewDelegate, UIActionSheetDelegate, ABPeoplePickerNavigationControllerDelegate, UIAlertViewDelegate, UITextFieldDelegate> {
}
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) IBOutlet MKMapView *locationsMapView;

@end
