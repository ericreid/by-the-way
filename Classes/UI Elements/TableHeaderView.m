//
//  TableHeaderView.h
//  ByTheWay
//
//  Created by Eric Reid on 3/6/10.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import "TableHeaderView.h"
#import "DrawingUtils.h"

static const CGFloat kHeaderHeight = 54.0;
static const CGFloat kHeaderOffset = 6.0;
static const CGFloat kTitleOffset = 12.0;

@interface TableHeaderView ()
@property (nonatomic, retain) UIButton *addButton;
@property (nonatomic, retain) UIColor *shadowColor;
@property (nonatomic, retain) UIColor *lightColor;
@property (nonatomic, retain) UIColor *darkColor;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic) CGRect headerRect; 
@end

@implementation TableHeaderView
@synthesize delegate;
@synthesize location;
@synthesize titleLabel;
@synthesize lightColor;
@synthesize darkColor;
@synthesize addButton;
@synthesize shadowColor;
@synthesize headerRect;

- (void)dealloc {
    [location release], location = nil;
    [titleLabel release], titleLabel = nil;
    [lightColor release], lightColor = nil;
    [darkColor release], darkColor = nil;
    [addButton release], addButton = nil;
    [shadowColor release], shadowColor = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

- (id)init {
    if ((self = [super init])) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
        
        self.titleLabel = [[[UILabel alloc] init] autorelease];
        self.titleLabel.opaque = NO;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:22.0];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        self.titleLabel.shadowOffset = CGSizeMake(0, -1);
        [self addSubview:self.titleLabel];
        
        UIImage *plusImage = [UIImage imageNamed:@"plus_button.png"];
        self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.addButton.frame = CGRectMake(0.0, 0.0, plusImage.size.width, plusImage.size.height);
        [self.addButton addTarget:self action:@selector(addTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.addButton setImage:plusImage forState:UIControlStateNormal];
        [self addSubview:self.addButton];
        
        self.lightColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1.0];
        self.darkColor = [UIColor colorWithRed:0.15 green:0.15 blue:0.15 alpha:1.0];    
        self.shadowColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:0.5]; 
    }
    return self;
}

- (void)setLocation:(Location *)newLocation {
    [location release], location = nil;
    location = [newLocation retain];
    self.titleLabel.text = newLocation.nickname;
    switch (newLocation.colorValue) {
        case kGreenCircle:
            self.lightColor = [UIColor colorWithRed:0.0/255.0 green:146.0/255.0 blue:0.0/255.0 alpha:1.0];
            self.darkColor = [UIColor colorWithRed:0.0/255.0 green:76.0/255.0 blue:0.0/255.0 alpha:1.0];
            break;
        case kPurpleCircle:
            self.lightColor = [UIColor colorWithRed:147.0/255.0 green:105.0/255.0 blue:216.0/255.0 alpha:1.0];
            self.darkColor = [UIColor colorWithRed:72.0/255.0 green:22.0/255.0 blue:137.0/255.0 alpha:1.0];
            break;
        case kRedCircle:
            self.lightColor = [UIColor colorWithRed:194.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
            self.darkColor = [UIColor colorWithRed:94.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }
}

- (void)layoutSubviews {
    self.headerRect = CGRectMake(kHeaderOffset, kHeaderOffset, self.bounds.size.width - (kHeaderOffset * 2), kHeaderHeight);
    self.titleLabel.frame = CGRectMake(kTitleOffset + self.headerRect.origin.x, self.headerRect.origin.y, self.headerRect.size.width, self.headerRect.size.height);
    
    self.addButton.frame = CGRectMake(self.headerRect.size.width - 45, (kHeaderHeight / 2) - (self.addButton.frame.size.height / 2) + kHeaderOffset, self.addButton.frame.size.width, self.addButton.frame.size.height);;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();    
    
    CGContextSaveGState(context);
    CGContextSetShadowWithColor(context, CGSizeMake(0, 2), 3.0, self.shadowColor.CGColor);
    CGContextSetFillColorWithColor(context, self.lightColor.CGColor);
    CGContextFillRect(context, self.headerRect);
    CGContextRestoreGState(context);
    
    [DrawingUtils drawGlossAndGradientInContext:context rect:self.headerRect startColor:self.lightColor.CGColor endColor:self.darkColor.CGColor];
    
    // Draw stroke
    CGContextSetStrokeColorWithColor(context, self.darkColor.CGColor);
    CGContextSetLineWidth(context, 1.0);    
    CGContextStrokeRect(context, [DrawingUtils rectFor1PointStrokeWithRect:self.headerRect]);
}

- (void)addTapped {
    [self.delegate addTappedForLocation:self.location];
}

@end
