// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Reminder.m instead.

#import "_Reminder.h"

@implementation ReminderID
@end

@implementation _Reminder

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Reminder" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Reminder";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Reminder" inManagedObjectContext:moc_];
}

- (ReminderID*)objectID {
	return (ReminderID*)[super objectID];
}




@dynamic subject;






@dynamic latest;



- (long long)latestValue {
	NSNumber *result = [self latest];
	return [result longLongValue];
}

- (void)setLatestValue:(long long)value_ {
	[self setLatest:[NSNumber numberWithLongLong:value_]];
}

- (long long)primitiveLatestValue {
	NSNumber *result = [self primitiveLatest];
	return [result longLongValue];
}

- (void)setPrimitiveLatestValue:(long long)value_ {
	[self setPrimitiveLatest:[NSNumber numberWithLongLong:value_]];
}





@dynamic entering;



- (BOOL)enteringValue {
	NSNumber *result = [self entering];
	return [result boolValue];
}

- (void)setEnteringValue:(BOOL)value_ {
	[self setEntering:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveEnteringValue {
	NSNumber *result = [self primitiveEntering];
	return [result boolValue];
}

- (void)setPrimitiveEnteringValue:(BOOL)value_ {
	[self setPrimitiveEntering:[NSNumber numberWithBool:value_]];
}





@dynamic earliest;



- (long long)earliestValue {
	NSNumber *result = [self earliest];
	return [result longLongValue];
}

- (void)setEarliestValue:(long long)value_ {
	[self setEarliest:[NSNumber numberWithLongLong:value_]];
}

- (long long)primitiveEarliestValue {
	NSNumber *result = [self primitiveEarliest];
	return [result longLongValue];
}

- (void)setPrimitiveEarliestValue:(long long)value_ {
	[self setPrimitiveEarliest:[NSNumber numberWithLongLong:value_]];
}





@dynamic location;

	





@end
