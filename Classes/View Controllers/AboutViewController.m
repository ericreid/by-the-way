//
//  AboutViewController.m
//  ByTheWay
//
//  Created by Eric Reid on 3/13/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import "AboutViewController.h"

static NSString* const kWebsiteUrl = @"http://www.strawmanstudios.com";
static NSString* const kSupportEmail = @"support@strawmanstudios.com";
static NSString* const kFeedbackSubject = @"I have a question about By The Way";

@implementation AboutViewController

- (void)dealloc {
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)back {
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)showWebsite {
    [FlurryAPI logEvent:kFlurryEventWebsiteTapped];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kWebsiteUrl]]; 
}

- (IBAction)emailSupport {
    [FlurryAPI logEvent:kFlurryEventFeedbackTapped];
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:kSupportEmail]];
        [controller setSubject:kFeedbackSubject];
        [self presentModalViewController:controller animated:YES];
        [controller release];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Can't send email" 
                                                        message:@"This device can't send email. Double check your email settings."
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [FlurryAPI logEvent:kFlurryEventFeedbackSent];
	[self becomeFirstResponder];
	[self dismissModalViewControllerAnimated:YES];
}

@end
