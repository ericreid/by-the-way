//
//  TableCellBackground.h
//  ByTheWay
//
//  Created by Eric Reid on 3/6/10.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import "TableCellBackground.h"
#import "DrawingUtils.h"

@interface TableCellBackground ()
@property (nonatomic, retain) UIColor *top;
@property (nonatomic, retain) UIColor *bottom;
@property (nonatomic, retain) UIColor *border;
@end

@implementation TableCellBackground
@synthesize top;
@synthesize bottom;
@synthesize border;

- (void)dealloc {
    [top release], top = nil;
    [bottom release], bottom = nil;
    [border release], border = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

- (id)initWithTopColor:(UIColor*)topColor bottomColor:(UIColor*)bottomColor borderColor:(UIColor*)borderColor {
    if ((self = [super init])) {
        self.top = topColor;
        self.bottom = bottomColor;
        self.border = borderColor;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();    
    CGRect cellRect = self.bounds;
    
    [DrawingUtils drawLinearGradientInContext:context rect:cellRect startColor:self.top.CGColor endColor:self.bottom.CGColor];
    
    CGRect strokeRect = cellRect;
    strokeRect.size.height -= 1;
    strokeRect = [DrawingUtils rectFor1PointStrokeWithRect:strokeRect];
    
    CGContextSetStrokeColorWithColor(context, self.border.CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextStrokeRect(context, strokeRect);
    
    CGPoint startPoint = CGPointMake(cellRect.origin.x, 
                                     cellRect.origin.y + cellRect.size.height - 1);
    CGPoint endPoint = CGPointMake(cellRect.origin.x + cellRect.size.width - 1, 
                                   cellRect.origin.y + cellRect.size.height - 1);
    
    [DrawingUtils draw1PointStrokeInContext:context startPoint:startPoint endPoint:endPoint color:self.border.CGColor];
}


@end
