//
//  LocationManager.h
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GeofenceDelegate;

@interface LocationManager : NSObject <CLLocationManagerDelegate> {
}
@property (nonatomic, assign) id<GeofenceDelegate> geofenceDelegate;
@property (nonatomic, retain) CLLocationManager *locationManager;

+ (LocationManager*)sharedInstance;

- (CLLocationCoordinate2D)currentCoordinate;
- (CLLocation*)currentLocation;
- (void)startUpdatingLocation;
- (void)stopUpdatingLocation;

- (NSArray*)allGeofences;
- (void)deleteGeofenceAtLatitude:(double)latitude longitude:(double)longitude;
- (void)saveGeofenceAtLatitude:(double)latitude longitude:(double)longitude radius:(double)radius;

@end

@protocol GeofenceDelegate <NSObject>
- (void)geofenceTriggered:(CLRegion*)geofence entered:(BOOL)entered;
@end