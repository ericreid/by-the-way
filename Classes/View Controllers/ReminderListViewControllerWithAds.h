//
//  ReminderListViewControllerWithAds.h
//  ByTheWay
//
//  Created by Eric Reid on 3/31/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReminderListViewController.h"
#import "AdWhirlView.h"

@interface ReminderListViewControllerWithAds : ReminderListViewController <AdWhirlDelegate>  {
    
}
@property (nonatomic, retain) IBOutlet UIView *adContainerView;

@end
