//
//  ByTheWayAppDelegate.h
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationManager.h"

@class ReminderListViewController;
@class LocationsViewController;

@interface ByTheWayAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate, GeofenceDelegate> {
    
@private
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) IBOutlet ReminderListViewController *reminderListViewController;
@property (nonatomic, retain) IBOutlet LocationsViewController *locationsViewController;

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
