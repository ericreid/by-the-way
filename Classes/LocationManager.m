//
//  LocationManager.m
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "LocationManager.h"

static float kMinDistance = 25.0;
static float kMinAge = 15.0;

static NSString* const kGeofencesFileName = @"geofences.btw";
static NSString* const kInsideFileName = @"inside.btw";

@interface LocationManager ()
@property (nonatomic, retain) NSMutableDictionary *geofences;
@property (nonatomic, retain) NSMutableDictionary *insideGeofence;
@end

@implementation LocationManager
@synthesize geofenceDelegate;
@synthesize locationManager;

@synthesize geofences;
@synthesize insideGeofence;

#pragma mark -
#pragma mark Singleton methods

+ (LocationManager*)sharedInstance {
    static dispatch_once_t pred;
    static LocationManager *locationManager = nil;
    
    dispatch_once(&pred, ^{ locationManager = [[self alloc] init]; });
    return locationManager;
}

#pragma mark -
#pragma mark class instance methods

- (id)init {
    if((self = [super init])) {
        NSString *documentsDirectory = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path];
        
        NSString *geofencesFileName = [documentsDirectory stringByAppendingPathComponent:kGeofencesFileName];
        NSString *insideFileName = [documentsDirectory stringByAppendingPathComponent:kInsideFileName];
        
        self.geofences = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithFile:geofencesFileName]];
        
        self.insideGeofence = [NSMutableDictionary dictionaryWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithFile:insideFileName]];
        

        [self startUpdatingLocation];
    }
    
    return self;
}

- (CLLocationCoordinate2D)currentCoordinate {
    return self.locationManager.location.coordinate;
}

- (CLLocation*)currentLocation {
    return self.locationManager.location;
}

- (void)startUpdatingLocation {
    if(!self.locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.distanceFilter = kMinDistance;
    }
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
}

- (void)saveFiles {
    NSString *documentsDirectory = [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] path];
    
    [NSKeyedArchiver archiveRootObject:self.geofences toFile:[documentsDirectory stringByAppendingPathComponent:kGeofencesFileName]];
    [NSKeyedArchiver archiveRootObject:self.insideGeofence toFile:[documentsDirectory stringByAppendingPathComponent:kInsideFileName]];
}

- (NSArray*)allGeofences {
    return [self.geofences allValues];
}

- (void)deleteGeofenceAtLatitude:(double)latitude longitude:(double)longitude {
    NSString *identifier = [NSString stringWithFormat:@"%f_%f", latitude, longitude];
    [self.geofences removeObjectForKey:identifier];
    [self.insideGeofence removeObjectForKey:identifier];
    [self saveFiles];
}

- (void)saveGeofenceAtLatitude:(double)latitude longitude:(double)longitude radius:(double)radius {
    NSString *identifier = [NSString stringWithFormat:@"%f_%f", latitude, longitude];    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    CLRegion *region = [[CLRegion alloc] initCircularRegionWithCenter:coordinate radius:radius identifier:identifier];
    [self.geofences setObject:region forKey:identifier];
    if([region containsCoordinate:self.currentCoordinate]) {
        [self.insideGeofence setObject:[NSNumber numberWithBool:YES] forKey:identifier];
    } else {
        [self.insideGeofence setObject:[NSNumber numberWithBool:NO] forKey:identifier];
    }
    [self saveFiles];
    [region release];
}

- (void)checkForGeofenceTriggers {
    for(CLRegion *region in [self.geofences allValues]) {
        BOOL inside = [[self.insideGeofence objectForKey:region.identifier] boolValue];
        if(inside) {
            if(![region containsCoordinate:self.currentCoordinate]) {
                DLog(@"Yay. We have now left a geofence: %@", region.identifier);
                [self.insideGeofence setObject:[NSNumber numberWithBool:NO] forKey:region.identifier];
                [self saveFiles];
                [self.geofenceDelegate geofenceTriggered:region entered:NO];
            } else {
                DLog(@"Already inside geofence");
            }
        } else {
            if([region containsCoordinate:self.currentCoordinate]) {
                DLog(@"Yay. We have now entered a geofence: %@", region.identifier);
                [self.insideGeofence setObject:[NSNumber numberWithBool:YES] forKey:region.identifier];
                [self saveFiles];
                [self.geofenceDelegate geofenceTriggered:region entered:YES];
            } else {
                DLog(@"Already outside geofence");
            }
        }
    }
}

#pragma mark -
#pragma mark CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSDate* eventDate = newLocation.timestamp;
    
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if(abs(howRecent) < kMinAge) {
        [FlurryAPI setLatitude:manager.location.coordinate.latitude longitude:manager.location.coordinate.longitude horizontalAccuracy:manager.location.horizontalAccuracy verticalAccuracy:manager.location.verticalAccuracy];
        [self checkForGeofenceTriggers];
    }

}

@end
