//
//  LocationDetailViewController.h
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationDetailViewController : UIViewController <UITextFieldDelegate, MKMapViewDelegate> {
}
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) IBOutlet UITextField *nicknameTextField;
@property (nonatomic, retain) IBOutlet UISlider *radiusSlider;
@property (nonatomic, retain) IBOutlet UILabel *radiusLabel;
@property (nonatomic, retain) IBOutlet MKMapView *previewMapView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *colorControl;

- (IBAction)radiusSliderMoved;
- (IBAction)hideKeyboard;
- (IBAction)colorChanged;

@end
