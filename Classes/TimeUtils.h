//
//  TimeUtils.h
//  ByTheWay
//
//  Created by Eric Reid on 1/3/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TimeUtils : NSObject {
}
+ (NSDate*)dateFromSecondsInDay:(NSUInteger)secondsInDay;
+ (NSUInteger)secondsInDayFromDate:(NSDate*)date;

@end
