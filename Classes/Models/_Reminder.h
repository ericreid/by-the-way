// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Reminder.h instead.

#import <CoreData/CoreData.h>


@class Location;






@interface ReminderID : NSManagedObjectID {}
@end

@interface _Reminder : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ReminderID*)objectID;



@property (nonatomic, retain) NSString *subject;

//- (BOOL)validateSubject:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *latest;

@property long long latestValue;
- (long long)latestValue;
- (void)setLatestValue:(long long)value_;

//- (BOOL)validateLatest:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *entering;

@property BOOL enteringValue;
- (BOOL)enteringValue;
- (void)setEnteringValue:(BOOL)value_;

//- (BOOL)validateEntering:(id*)value_ error:(NSError**)error_;



@property (nonatomic, retain) NSNumber *earliest;

@property long long earliestValue;
- (long long)earliestValue;
- (void)setEarliestValue:(long long)value_;

//- (BOOL)validateEarliest:(id*)value_ error:(NSError**)error_;




@property (nonatomic, retain) Location* location;
//- (BOOL)validateLocation:(id*)value_ error:(NSError**)error_;




@end

@interface _Reminder (CoreDataGeneratedAccessors)

@end

@interface _Reminder (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveSubject;
- (void)setPrimitiveSubject:(NSString*)value;


- (NSNumber*)primitiveLatest;
- (void)setPrimitiveLatest:(NSNumber*)value;

- (long long)primitiveLatestValue;
- (void)setPrimitiveLatestValue:(long long)value_;


- (NSNumber*)primitiveEntering;
- (void)setPrimitiveEntering:(NSNumber*)value;

- (BOOL)primitiveEnteringValue;
- (void)setPrimitiveEnteringValue:(BOOL)value_;


- (NSNumber*)primitiveEarliest;
- (void)setPrimitiveEarliest:(NSNumber*)value;

- (long long)primitiveEarliestValue;
- (void)setPrimitiveEarliestValue:(long long)value_;




- (Location*)primitiveLocation;
- (void)setPrimitiveLocation:(Location*)value;


@end
