//
//  LocationsViewController.m
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "LocationsViewController.h"
#import "LocationDetailViewController.h"
#import "LocationMapOverlay.h"
#import "LocationAnnotation.h"

static const double kViewSpanDelta = 0.01;
static const int kUpgradeAlertViewTag = 42;
static const int kAddAddressAlertViewTag = 1;

static NSString* const kFullVersionItunesUrl = @"http://itunes.apple.com/app/by-the-way/id406895211";

@interface LocationsViewController ()
@property (nonatomic, retain) UIAlertView *addressAlert;
@property (nonatomic, retain) UITextField *addressField;

- (void)addTapped;
- (void)addIfPossibleForFreeVersion;
- (void)addNewLocation;

- (void)drawLocationsOnMap;
- (CLLocationCoordinate2D)addressLocation:(NSString*)address;
- (void)addAtCoordinate:(CLLocationCoordinate2D)coordinate;
@end

@implementation LocationsViewController
@synthesize managedObjectContext;
@synthesize tabBarController;
@synthesize locationsMapView;

@synthesize addressAlert;
@synthesize addressField;

- (void)dealloc {
    [managedObjectContext release], managedObjectContext = nil;
    [tabBarController release], tabBarController = nil;
    [locationsMapView release], locationsMapView = nil;
    
    [addressAlert release], addressAlert = nil;
    [addressField release], addressField = nil;
    [super dealloc];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Don't unload tabBarController, since it comes from the main window
    self.locationsMapView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTapped)] autorelease];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self drawLocationsOnMap];
}

- (void)drawLocationsOnMap {
    [self.locationsMapView removeAnnotations:self.locationsMapView.annotations];
    [self.locationsMapView removeOverlays:self.locationsMapView.overlays];
    
    NSArray *locations = [Location allLocationsInManagedObjectContext:self.managedObjectContext];
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    if(locations.count == 0) {
        span.latitudeDelta = kViewSpanDelta;
        span.longitudeDelta = kViewSpanDelta;    
        
        region.span = span;
        region.center = [LocationManager sharedInstance].currentCoordinate;
    } else {
        double minLatitude = 180;
        double maxLatitude = -180;
        double minLongitude = 180;
        double maxLongitude = -180;
        
        for (Location* location in locations) {
            if(location.latitudeValue < minLatitude) {
                minLatitude = location.latitudeValue;
            }
            if(location.latitudeValue > maxLatitude) {
                maxLatitude = location.latitudeValue;
            }
            if(location.longitudeValue < minLongitude) {
                minLongitude = location.longitudeValue;
            }
            if(location.longitudeValue > maxLongitude) {
                maxLongitude = location.longitudeValue;
            }
            
            LocationMapOverlay *mapOverlay = [[LocationMapOverlay alloc] initWithLocation:location];
            mapOverlay.color = location.colorValue;
            [self.locationsMapView addOverlay:mapOverlay];
            [mapOverlay release];
            
            LocationAnnotation *annotation = [[LocationAnnotation alloc] initWithCoordinate:location.coordinate];
            [self.locationsMapView addAnnotation:annotation];
            annotation.location = location;
            annotation.title = location.nickname;
            [annotation release];
        }
        
        if(locations.count == 1) {
            span.latitudeDelta = kViewSpanDelta;
            span.longitudeDelta = kViewSpanDelta;    
            
            region.span = span;
            Location *location = [locations objectAtIndex:0];
            region.center = location.coordinate;
        } else {
            span.latitudeDelta=maxLatitude - minLatitude + ((maxLatitude - minLatitude) * 0.1);
            span.longitudeDelta=maxLongitude - minLongitude + ((maxLongitude - minLongitude) * 0.1);
        
            region.span=span;
            region.center = CLLocationCoordinate2DMake(minLatitude + (span.latitudeDelta / 2), minLongitude + (span.longitudeDelta / 2));
        }
    }
    
    [self.locationsMapView setRegion:region animated:NO];
}

- (void)addTapped {
#ifdef FREE_VERSION
    [self addIfPossibleForFreeVersion];
#else
    [self addNewLocation];
#endif
}

- (void)addIfPossibleForFreeVersion {
    NSArray *locations = [Location allLocationsInManagedObjectContext:self.managedObjectContext];
    if(locations.count == 0) {
        [self addNewLocation];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location limit exceeded" message:@"You are limited to only one location in the free version of By The Way. Would you like to upgrade to the full version?" delegate:self cancelButtonTitle:@"No Thanks" otherButtonTitles:@"Upgrade", nil];
        alertView.tag = kUpgradeAlertViewTag;
        [alertView show];
        [alertView release];
    }
}

- (void)addNewLocation {
    [FlurryAPI logEvent:kFlurryEventAddLocationTapped];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                             delegate:self 
                                                    cancelButtonTitle:@"Cancel" 
                                               destructiveButtonTitle:nil 
                                                    otherButtonTitles:@"Add at current location", @"Add at center of map", @"Add from contact", @"Add from address", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
    [actionSheet release];
}

- (void)addAtCoordinate:(CLLocationCoordinate2D)coordinate {
    Location *newLocation = [Location insertInManagedObjectContext:self.managedObjectContext];
    newLocation.coordinate = coordinate;
    newLocation.radiusValue = 50;
    
    LocationDetailViewController *controller = [[LocationDetailViewController alloc] init];
    controller.managedObjectContext = self.managedObjectContext;
    controller.location = newLocation;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)addFromContact {
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.displayedProperties = [NSArray arrayWithObject:[NSNumber numberWithInt:kABPersonAddressProperty]];
    picker.peoplePickerDelegate = self;
    [self presentModalViewController:picker animated:YES];
    [picker release];
}

- (void)addFromAddress {
    [addressAlert release];
    addressAlert = [[UIAlertView alloc] initWithTitle:@"Address" 
                                                           message:@"\n"
                                                           delegate:self 
                                                 cancelButtonTitle:@"Cancel" 
                                                 otherButtonTitles:@"OK", nil];
    
    [addressField release];
    addressField = [[UITextField alloc] initWithFrame:CGRectMake(16,45,252,25)];
    self.addressField.borderStyle = UITextBorderStyleRoundedRect;
    self.addressField.keyboardAppearance = UIKeyboardAppearanceAlert;
    self.addressField.delegate = self;
    self.addressField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.addressField becomeFirstResponder];
    [self.addressAlert addSubview:addressField];

    self.addressAlert.tag = kAddAddressAlertViewTag;
    [addressAlert show];
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == kAddAddressAlertViewTag && 1 == buttonIndex) {
        [self addAtCoordinate:[self addressLocation:self.addressField.text]];
    } else if(alertView.tag == kUpgradeAlertViewTag && 1 == buttonIndex) {
        NSURL *upgradeUrl = [NSURL URLWithString:kFullVersionItunesUrl];
        [[UIApplication sharedApplication] openURL:upgradeUrl];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.addressAlert dismissWithClickedButtonIndex:1 animated:YES];
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [FlurryAPI logEvent:kFlurryEventAddLocationFromCurrentPosition];
            [self addAtCoordinate:self.locationsMapView.userLocation.coordinate];
            break;
        case 1:
            [FlurryAPI logEvent:kFlurryEventAddLocationFromMapCenter];
            [self addAtCoordinate:self.locationsMapView.centerCoordinate];
            break;
        case 2:
            [FlurryAPI logEvent:kFlurryEventAddLocationFromContact];
            [self addFromContact];
            break;
        case 3:
            [FlurryAPI logEvent:kFlurryEventAddLocationFromAddress];
            [self addFromAddress];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark ABPeoplePickerNavigationControllerDelegate

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person {
    return YES;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef addressProperty = ABRecordCopyValue(person, property);
    CFTypeRef cfDict = ABMultiValueCopyValueAtIndex(addressProperty, identifier);
	NSDictionary *theDict = [[(NSDictionary*)cfDict retain] autorelease];
    CFRelease(cfDict);
    CFRelease(addressProperty);
    
    NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@",
                         [theDict objectForKey:(NSString *)kABPersonAddressStreetKey],
                         [theDict objectForKey:(NSString *)kABPersonAddressCityKey],
                         [theDict objectForKey:(NSString *)kABPersonAddressStateKey],
                         [theDict objectForKey:(NSString *)kABPersonAddressZIPKey]];                               
    
    [self addAtCoordinate:[self addressLocation:address]];
    [self dismissModalViewControllerAnimated:YES];
    return NO;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker {
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Address lookup

- (CLLocationCoordinate2D)addressLocation:(NSString*)address {
    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv",
                           [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSUTF8StringEncoding error:nil];
    NSArray *listItems = [locationString componentsSeparatedByString:@","];
    
    double latitude = 0.0;
    double longitude = 0.0;
    
    if([listItems count] >= 4 && [[listItems objectAtIndex:0] isEqualToString:@"200"]) {
        latitude = [[listItems objectAtIndex:2] doubleValue];
        longitude = [[listItems objectAtIndex:3] doubleValue];
    }
    
    CLLocationCoordinate2D location;
    location.latitude = latitude;
    location.longitude = longitude;
    
    return location;
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    if ([overlay isKindOfClass:[LocationMapOverlay class]]) {
        LocationMapOverlay* locationOverlay = (LocationMapOverlay*)overlay;
        return [locationOverlay viewForOverlay];
    }
    
    return nil;
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
    if(annotation != self.locationsMapView.userLocation)
    {
        LocationAnnotation *locationAnnotation = (LocationAnnotation*)annotation;
        static NSString *viewIdentifier = @"annotationView";
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:viewIdentifier];
        if (annotationView == nil) {
            annotationView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewIdentifier] autorelease];
        }
        switch (locationAnnotation.location.colorValue) {
            case kGreenCircle:
                annotationView.pinColor = MKPinAnnotationColorGreen;
                break;
            case kPurpleCircle:
                annotationView.pinColor = MKPinAnnotationColorPurple;
                break;
            case kRedCircle:
                annotationView.pinColor = MKPinAnnotationColorRed;
                break;
            default:
                break;
        }
        
        annotationView.animatesDrop = NO;
        annotationView.canShowCallout = YES;
        annotationView.calloutOffset = CGPointMake(-8, 5);
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];   
        return annotationView;
    } else {
        return nil;
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    LocationDetailViewController *controller = [[LocationDetailViewController alloc] init];
    controller.managedObjectContext = self.managedObjectContext;
    controller.location = [(LocationAnnotation*)view.annotation location];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

@end
