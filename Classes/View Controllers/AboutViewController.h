//
//  AboutViewController.h
//  ByTheWay
//
//  Created by Eric Reid on 3/13/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
}

- (IBAction)showWebsite;
- (IBAction)emailSupport;
- (IBAction)back;

@end
