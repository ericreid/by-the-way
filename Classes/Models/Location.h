#import "_Location.h"

@interface Location : _Location {}
@property (nonatomic) CLLocationCoordinate2D coordinate;

+ (NSArray*)allLocationsInManagedObjectContext:(NSManagedObjectContext*)context;
+ (Location*)locationAtLatitude:(double)latitude longitude:(double)longitude inManagedObjectContext:(NSManagedObjectContext*)context;

@end
