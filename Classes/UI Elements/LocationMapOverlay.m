//
//  LocationMapOverlay.m
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import "LocationMapOverlay.h"

@interface LocationMapOverlay ()
@property (nonatomic, retain) MKCircle *circle;
@property (nonatomic, retain) Location *location;
@end

@implementation LocationMapOverlay 
@synthesize circle;
@synthesize location;
@synthesize color;

- (void)dealloc {
    [circle release], circle = nil;
    [location release], location = nil;
    [super dealloc];
}

- (id)initWithLocation:(Location*)loc {
    if((self = [super init])) {
        self.location = loc;
        
        CLLocationCoordinate2D center;
        center.latitude = self.location.latitudeValue;
        center.longitude = self.location.longitudeValue;
        
        self.circle = [MKCircle circleWithCenterCoordinate:center radius:self.location.radiusValue];
    }
    return self;    
}

- (MKCircleView*)viewForOverlay {
    
    MKCircleView *view = [[[MKCircleView alloc] initWithCircle:self.circle] autorelease];
    
    switch (color) {
        case kGreenCircle:
            view.fillColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.4];
            view.strokeColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0];
            break;
        case kPurpleCircle:
            view.fillColor = [UIColor colorWithRed:1.0 green:0.0 blue:1.0 alpha:0.4];
            view.strokeColor =  [UIColor colorWithRed:1.0 green:0.0 blue:1.0 alpha:1.0]; 
            break;
        case kRedCircle:
            view.fillColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.4];
            view.strokeColor =  [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0]; 
            break;
        default:
            break;
    }
    
    view.lineWidth = 4.0;
    return view;  
}

// defer to the circle object we have inside
- (BOOL)intersectsMapRect:(MKMapRect)mapRect {
    return [self.circle intersectsMapRect:mapRect];
}

// these properties are not synthesized so that they can be passed to the real ciricle
- (MKMapRect)boundingMapRect {
    return [self.circle boundingMapRect];
}

- (CLLocationCoordinate2D)coordinate {
    return [self.circle coordinate];
}

@end
