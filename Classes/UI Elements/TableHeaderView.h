//
//  TableHeaderView.h
//  ByTheWay
//
//  Created by Eric Reid on 3/6/10.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableHeaderDelegate;

@interface TableHeaderView : UIView {

}
@property (nonatomic, assign) id<TableHeaderDelegate> delegate;
@property (nonatomic, retain) Location *location;

@end

@protocol TableHeaderDelegate <NSObject>
- (void)addTappedForLocation:(Location*)location;
@end
