// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Location.m instead.

#import "_Location.h"

@implementation LocationID
@end

@implementation _Location

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Location";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Location" inManagedObjectContext:moc_];
}

- (LocationID*)objectID {
	return (LocationID*)[super objectID];
}




@dynamic nickname;






@dynamic longitude;



- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:[NSNumber numberWithDouble:value_]];
}





@dynamic latitude;



- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithDouble:value_]];
}





@dynamic color;



- (short)colorValue {
	NSNumber *result = [self color];
	return [result shortValue];
}

- (void)setColorValue:(short)value_ {
	[self setColor:[NSNumber numberWithShort:value_]];
}

- (short)primitiveColorValue {
	NSNumber *result = [self primitiveColor];
	return [result shortValue];
}

- (void)setPrimitiveColorValue:(short)value_ {
	[self setPrimitiveColor:[NSNumber numberWithShort:value_]];
}





@dynamic radius;



- (short)radiusValue {
	NSNumber *result = [self radius];
	return [result shortValue];
}

- (void)setRadiusValue:(short)value_ {
	[self setRadius:[NSNumber numberWithShort:value_]];
}

- (short)primitiveRadiusValue {
	NSNumber *result = [self primitiveRadius];
	return [result shortValue];
}

- (void)setPrimitiveRadiusValue:(short)value_ {
	[self setPrimitiveRadius:[NSNumber numberWithShort:value_]];
}





@dynamic reminders;

	
- (NSMutableSet*)remindersSet {
	[self willAccessValueForKey:@"reminders"];
	NSMutableSet *result = [self mutableSetValueForKey:@"reminders"];
	[self didAccessValueForKey:@"reminders"];
	return result;
}
	





@end
