//
//  DrawingUtils.h
//  ByTheWay
//
//  Created by Eric Reid on 3/6/10.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DrawingUtils : NSObject {
}

+ (void)drawLinearGradientInContext:(CGContextRef)context rect:(CGRect)rect startColor:(CGColorRef)startColor endColor:(CGColorRef)endColor;
+ (void)drawGlossAndGradientInContext:(CGContextRef)context rect:(CGRect)rect startColor:(CGColorRef)startColor endColor:(CGColorRef)endColor;
+ (CGRect)rectFor1PointStrokeWithRect:(CGRect)rect;
+ (void)draw1PointStrokeInContext:(CGContextRef)context startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint color:(CGColorRef)color;

@end
