//
//  ReminderListViewController.h
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableHeaderView.h"

@interface ReminderListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, TableHeaderDelegate> {

}
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) IBOutlet UILabel *noRemindersLabel;
@property (nonatomic, retain) IBOutlet UITableView *remindersTableView;

@end