//
//  TimeUtils.m
//  ByTheWay
//
//  Created by Eric Reid on 1/3/11.
//  Copyright 2011 Strawman Studios. All rights reserved.
//

#import "TimeUtils.h"


@implementation TimeUtils

+ (NSDate*)dateFromSecondsInDay:(NSUInteger)secondsInDay {
    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:flags fromDate:[NSDate date]];
    NSDate* today = [calendar dateFromComponents:components];
    return [today dateByAddingTimeInterval:secondsInDay];
}

+ (NSUInteger)secondsInDayFromDate:(NSDate*)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"A"];
    NSUInteger secondsInDay = [[formatter stringFromDate:date] intValue] / 1000;
    [formatter release];
    return secondsInDay;
}

@end
