//
//  LocationMapOverlay.h
//  ByTheWay
//
//  Created by Eric Reid on 12/5/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationMapOverlay : NSObject <MKOverlay> {

}
@property (nonatomic, readonly) MKMapRect boundingMapRect;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) CircleColor color;

- (id)initWithLocation:(Location*)loc;
- (MKCircleView*)viewForOverlay;

@end
