//
//  ReminderDetailViewController.h
//  ByTheWay
//
//  Created by Eric Reid on 12/11/10.
//  Copyright 2010 Strawman Studios. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ReminderDetailViewController : UIViewController {
}
@property (nonatomic, retain) Reminder *reminder;
@property (nonatomic, retain) IBOutlet UIButton *arriveButton;
@property (nonatomic, retain) IBOutlet UIButton *earliestAnytimeButton;
@property (nonatomic, retain) IBOutlet UIButton *earliestSpecificButton;
@property (nonatomic, retain) IBOutlet UIButton *latestAnytimeButton;
@property (nonatomic, retain) IBOutlet UIButton *latestSpecificButton;
@property (nonatomic, retain) IBOutlet UIButton *leaveButton;
@property (nonatomic, retain) IBOutlet UIDatePicker *timePicker;
@property (nonatomic, retain) IBOutlet UITextField *subjectField;

- (IBAction)arriveTapped;
- (IBAction)backgroundTapped;
- (IBAction)earliestAnytimeTapped;
- (IBAction)earliestSpecificTapped;
- (IBAction)latestAnytimeTapped;
- (IBAction)latestSpecificTapped;
- (IBAction)leaveTapped;
- (IBAction)timePickerChanged;

@end
